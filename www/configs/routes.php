<?php
return array (
    'user/add' =>
        array (
            'title' => 'Добавление пользователя',
            'controller' => 'user',
            'action' => 'add'
        ),
    'user/list' =>
        array (
            'title' => 'Добавление пользователя',
            'controller' => 'user',
            'action' => 'list'
        ),
    'login' =>
        array(
            'title' => 'Вход пользователя',
            'controller' => 'user',
            'action' => 'login'
        )
);