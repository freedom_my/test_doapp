<?php

namespace core;

abstract class Model
{
    protected $data;
    protected $path;

    public function __construct()
    {
        $this->get();
    }

    public function get()
    {
        $this->data = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].$this->path),true);
    }

    public function returnData()
    {
        return $this->data;
    }

    public function set()
    {
        file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->path,json_encode($this->data));
    }

    public function check($login)
    {

        foreach ($this->data as $key => $item) {
            if ($item['login'] == $login) {
                return true;
            }
        }
        return false;
    }

    public function add($data) {
        $this->data[] = $data;
    }

    public function checkPassword($login,$password) {
        foreach ($this->data as $key => $item)
        {
            if ($item['login'] == $login and $item['password'] == $password) {
                return ['user_id'=>$key];
            }
        }
        return false;
    }
}