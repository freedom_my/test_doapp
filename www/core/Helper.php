<?php

namespace core;

class Helper
{
    public static function getControllerName($params)
    {
        return  'controllers\\'.ucfirst($params['controller']).'Controller';
    }

    public static function getActionName($params,$className)
    {
        return $params['action'].'Action';
    }
}