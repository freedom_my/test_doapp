<?php

namespace core;

class Router
{
    protected $routes = [];
    protected $params = [];
    protected $controller;
    protected $action;
    protected $config = 'configs/routes.php';

    public function __construct()
    {
        $arr = require_once($this->config);
        foreach ($arr as $key => $val){
            $this->add($key,$val);
        }
    }

    public function add($route,$params)
    {
        $this-> routes[$route] = $params;
    }

    public function match()
    {
        $url =  explode('?',trim($_SERVER['REQUEST_URI'],'/'))[0];
        foreach ($this->routes as $route => $params){
            if (strpos($url, $route) !== false) {
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    public function run()
    {
        if ($this->match()) {
            $controllerName = Helper::getControllerName($this->params);
            if ($controllerName) {
                $actionName = Helper::getActionName($this->params,$controllerName);
                if ($actionName) {
                    $controller = new $controllerName($this->params);
                    $controller->$actionName();
                } else {
                    echo 'Нет метода';
                }
            } else {
                echo 'Нет класса';
            }
        } else {
            header("Location: /login/");
        };
    }


}