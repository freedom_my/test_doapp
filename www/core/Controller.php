<?php

namespace core;

class Controller
{
    public $route;
    public $view;
    public $model;

    public function __construct($route)
    {
        if (isset($_GET['LOGOUT'])){
            session_destroy();
            header("Location: /login/");
        }
        $this->route = $route;
        $this->view = new View($route);
        $this->model = $this->loadModel($route['controller']);
    }

    public function loadModel($name)
    {
        $path = 'models\\'.ucfirst($name);
        if (class_exists($path)) {
            return new $path;
        }
    }
}