<?php

namespace controllers;

use core\Controller;

class UserController extends Controller
{
    public function addAction()
    {
        if ( empty($_SESSION['auth'])) {
            header('Location: /login/');
        }
        $vars['text'] = '';
        if (!empty($_POST)) {
            $check = $this->model->check($_POST['login']);
            if (!$check) {
                $data = [
                    'login' => $_POST['login'],
                    'password' => $_POST['password'],
                    'surname' => $_POST['surname'],
                    'name' => $_POST['name'],
                    'age' => $_POST['age']
                ];
                $this->model->add($data);
                $this->model->set();
                $vars['text'] = 'Пользователь успешно добавлен';
            } else {
                $vars['text'] = 'Такой пользователь уже есть';
            }
        }
        $this->view->render($this->route['title'],$vars);
    }

    public function listAction()
    {
        if ( empty($_SESSION['auth'])) {
            header('Location: /login/');
        }
        $data = $this->model->returnData();
        $this->view->render($this->route['title'],$data);
    }

    public function loginAction()
    {
        $vars['text'] = '';
        if (!empty($_POST)) {
            $check = $this->model->checkPassword($_POST['login'],$_POST['password']);
            if (!empty($check)) {
                $_SESSION['auth'] = true;
                $_SESSION['user_id'] = $check['user_id'];
            } else {
                $vars['text'] = 'Неправильный логин или пароль';
            }
        }
        if ( $_SESSION['auth']) {
            header('Location: /user/list/');
        }
        $this->view->render($this->route['title'],$vars);
    }
}